import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CalendarioProfiloComponent } from './calendario-profilo.component';

describe('CalendarioProfiloComponent', () => {
  let component: CalendarioProfiloComponent;
  let fixture: ComponentFixture<CalendarioProfiloComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CalendarioProfiloComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarioProfiloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
