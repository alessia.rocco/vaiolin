import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SidebarComponent } from './sidebar/sidebar.component';

import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HeaderComponent } from './header/header.component';
import { PaginaInizialeComponent } from './pagina-iniziale/pagina-iniziale.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { AutenticazioneComponent } from './autenticazione/autenticazione.component';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatCardModule } from '@angular/material/card';


import{ FirebaseTSApp } from 'firebasets/firebasetsApp/firebaseTSApp';
import { environment } from 'src/environments/environment';
import { EmailVerificationComponent } from './email-verification/email-verification.component';
import{ ProfileComponent} from './profile/profile.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { CreatePostComponent } from './create-post/create-post.component';
import { MatTabsModule } from '@angular/material/tabs';
import { PostComponent } from './post/post.component';
import { ClassificaComponent } from './classifica/classifica.component';
import { ProfiloUserComponent } from './profilo-user/profilo-user.component';
import { CalendarioProfiloComponent } from './calendario-profilo/calendario-profilo.component';
import { PhotoOpenComponent } from './photo-open/photo-open.component';
import { Offerta1Component } from './offerta1/offerta1.component';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import {MatInputModule} from '@angular/material/input';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { EventiComponent } from './eventi/eventi.component';
import { SchedaEventoComponent } from './scheda-evento/scheda-evento.component';
import {MatSliderModule} from '@angular/material/slider';
import { NgxSliderModule } from '@angular-slider/ngx-slider';
import { PaginaEventoComponent } from './pagina-evento/pagina-evento.component';
import { FotoEventoCliccataComponent } from './foto-evento-cliccata/foto-evento-cliccata.component';
import { PaginaInserzioniComponent } from './pagina-inserzioni/pagina-inserzioni.component';
import { SchedaInserzioniComponent } from './scheda-inserzioni/scheda-inserzioni.component';
import {MatBadgeModule} from '@angular/material/badge';
import { PaginaInserzioneComponent } from './pagina-inserzione/pagina-inserzione.component';
import { PostProfiloUserComponent } from './post-profilo-user/post-profilo-user.component';
import { InserzioneApertaComponent } from './inserzione-aperta/inserzione-aperta.component';
import { PaginaOfferteComponent } from './pagina-offerte/pagina-offerte.component';
import { SchedaOffertaFattaComponent } from './scheda-offerta-fatta/scheda-offerta-fatta.component';






















@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    HeaderComponent,
    PaginaInizialeComponent,
    AutenticazioneComponent,
    EmailVerificationComponent,
    ProfileComponent,
    CreatePostComponent,
    PostComponent,
    ClassificaComponent,
    ProfiloUserComponent,
    CalendarioProfiloComponent,
    PhotoOpenComponent,
    Offerta1Component,
    EventiComponent,
    SchedaEventoComponent,
    PaginaEventoComponent,
    FotoEventoCliccataComponent,
    PaginaInserzioniComponent,
    SchedaInserzioniComponent,
    PaginaInserzioneComponent,
    PostProfiloUserComponent,
    InserzioneApertaComponent,
    PaginaOfferteComponent,
    SchedaOffertaFattaComponent,
    
    
    
    
    
    
    
    
    
    
    
    
    
    
  ],


  

  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    NgbModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatBottomSheetModule,
    MatCardModule,
    MatDialogModule,
    MatIconModule,
    MatTabsModule,
    NgxMaterialTimepickerModule,
    MatInputModule,
    MatDatepickerModule,
    MatCheckboxModule,
    MatSliderModule,
    NgxSliderModule,
    MatBadgeModule
    
    
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor(){
    FirebaseTSApp.init(environment.firebaseConfig);
  }

 }
