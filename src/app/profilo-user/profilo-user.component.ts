import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Offerta1Component } from '../offerta1/offerta1.component';



@Component({
  selector: 'app-profilo-user',
  templateUrl: './profilo-user.component.html',
  styleUrls: ['./profilo-user.component.css'],

  encapsulation: ViewEncapsulation.None
})
export class ProfiloUserComponent implements OnInit {

  

  constructor(private dialogTrigger: MatDialog) { }

  ngOnInit(): void {
  }

  

  

  bottoneOffertaCliccato(){
    let dialogTriggerRef = this.dialogTrigger.open(Offerta1Component);
  }



}
