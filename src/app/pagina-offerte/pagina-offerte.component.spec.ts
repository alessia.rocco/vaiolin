import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaginaOfferteComponent } from './pagina-offerte.component';

describe('PaginaOfferteComponent', () => {
  let component: PaginaOfferteComponent;
  let fixture: ComponentFixture<PaginaOfferteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaginaOfferteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginaOfferteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
