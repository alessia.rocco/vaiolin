import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PhotoOpenComponent } from './photo-open.component';

describe('PhotoOpenComponent', () => {
  let component: PhotoOpenComponent;
  let fixture: ComponentFixture<PhotoOpenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PhotoOpenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PhotoOpenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
