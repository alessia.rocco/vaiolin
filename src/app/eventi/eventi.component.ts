import { Component, OnInit } from '@angular/core';
import { Options } from "@angular-slider/ngx-slider";
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-eventi',
  templateUrl: './eventi.component.html',
  styleUrls: ['./eventi.component.css']
})
export class EventiComponent implements OnInit {


  model!: NgbDateStruct;

  value: number = 1;
  highValue: number = 24;
  options: Options = {
    floor: 1,
    ceil: 24
  };

  formatLabel(value: number) {
    if (value >= 1000) {
      return Math.round(value / 1000) + 'km';
    }

    return value;
  }


  formatLabel2(value: number) {
    if (value >= 1000) {
      return Math.round(value / 1000) + '€';
    }

    return value;
  }
  

  constructor() { }

  ngOnInit(): void {
  }

}
