import { Component, OnInit } from '@angular/core';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { AutenticazioneComponent } from '../autenticazione/autenticazione.component';
import { FirebaseTSAuth} from 'firebasets/firebasetsAuth/firebaseTSAuth';



@Component({
  selector: 'app-pagina-iniziale',
  templateUrl: './pagina-iniziale.component.html',
  styleUrls: ['./pagina-iniziale.component.css']
})
export class PaginaInizialeComponent implements OnInit {

  state= AutenticazioneCompState.LOGIN;
  firebasetsAuth!: FirebaseTSAuth; 

  constructor(private Loginsheet: MatBottomSheet, ) { 
    this.firebasetsAuth = new FirebaseTSAuth();
  }

  ngOnInit(): void {
  }

  onResetClick(resetEmail:HTMLInputElement){

    let email = resetEmail.value;
    if(this.isNotEmpty(email)){
      this.firebasetsAuth.sendPasswordResetEmail(
        {
          email: email,
          onComplete: (err) => {
           
          }
        }
      );
    }

  }

  onLogin(
    loginEmail: HTMLInputElement,
    loginPassword: HTMLInputElement,
  ){
   
    let email = loginEmail.value;
    let password = loginPassword.value;

    if(this.isNotEmpty(email) && this.isNotEmpty(password)){
      this.firebasetsAuth.signInWith(
        {
          email:email,
          password:password,
          onComplete: (uc) => {
            
          },

          onFail: (err) => {

            alert(err)

          }
        }
      );
    }

  }

  onRegisterClick(

    registerEmail:HTMLInputElement,
    registerPassword:HTMLInputElement,
    registerConfirmPassword:HTMLInputElement,


  ){
    let email= registerEmail.value;
    let password= registerPassword.value;
    let confirmPassword= registerConfirmPassword.value;

    if(
      this.isNotEmpty(email) &&
      this.isNotEmpty(password) &&
      this.isNotEmpty(confirmPassword) &&
      this.isMatch(password, confirmPassword)
    ){

      this.firebasetsAuth.createAccountWith(
        {
          email:email,
          password: password,
          onComplete: (uc) => {
            alert("Account Creato");
            registerEmail.value="";
            registerPassword.value="";
            registerConfirmPassword.value="";
          },

          onFail: (err) => {
  
            alert("Creazione nuovo account non riuscito")
  
          }
        }
      );

    }
    
  }

  Iniziaclick(){


    console.log("ciao")
    this.Loginsheet.open(AutenticazioneComponent)
    ;
    


  }


  isNotEmpty(text:string){

    return text != null && text.length > 0;

  }

  isMatch(text:string, comparedwith:string){

    return text == comparedwith;

  }

  onForgotPasswordClick(){

    this.state = AutenticazioneCompState.FORGOT_PASSWORD;
    
    

  }

  onCreateAccountClick(){

    this.state = AutenticazioneCompState.REGISTER;
    console.log("ciao1")

  }

  onLoginClick(){

    this.state = AutenticazioneCompState.LOGIN;

  }

  


  isLoginState(){
    return this.state == AutenticazioneCompState.LOGIN;
  }

  isRegisterState(){
    return this.state == AutenticazioneCompState.REGISTER;
  }

  isForgotPasswordState(){
    return this.state == AutenticazioneCompState.FORGOT_PASSWORD;
  }


  getStateText(){

    switch(this.state){
      case AutenticazioneCompState.LOGIN:
        return "Login";
      case AutenticazioneCompState.FORGOT_PASSWORD:
        return "Password Dimenticata";
      case AutenticazioneCompState.REGISTER:
        return "Registrati";
    }

  }

}



export enum AutenticazioneCompState{
  LOGIN,
  REGISTER,
  FORGOT_PASSWORD
}
