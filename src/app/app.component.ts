
import { Component } from '@angular/core';

import { EmailVerificationComponent } from './email-verification/email-verification.component';

import { FirebaseTSAuth } from 'firebasets/firebasetsAuth/firebaseTSAuth';

import { Router } from '@angular/router';

import { FirebaseTSFirestore } from 'firebasets/firebasetsFirestore/firebaseTSFirestore';

import { MatDialog } from '@angular/material/dialog';


import { CreatePostComponent } from './create-post/create-post.component';




@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  
  closeResult!: String;

  
  
  firestore= new FirebaseTSFirestore ();

  userHasProfile=true;


  userDocument!: UserDocument;

  
  

  

  constructor(
    
    private router:Router,

    private dialog: MatDialog,
    
    ) {
    this.auth.listenToSignInStateChanges(
      user=> {
        this.auth.checkSignInState(
          {
            whenSignedIn: user => {

              
              
              
              

            },

            whenSignedOut: user => {

              

            },

            whenSignedInAndEmailNotVerified: user => {


              

                this.userHasProfile=!true;

                this.router.navigate(["emailVerification"]);

                console.log(this.userHasProfile)

                

                console.log(this.userHasProfile)

              

              
              

            },

            whenSignedInAndEmailVerified: user => {


              
              
              this.getUserProfile();
              console.log(this.userHasProfile)
              this.userHasProfile=true;

             

            },

            whenChanged: user => {

             

            }
          } 
        )
      }
        
    );
   }


   getUserProfile(){

    this.firestore.listenToDocument(
      {
        name:"Getting Document",
        path: ["Users"],
        onUpdate: (result) => {

          this.userDocument = <UserDocument> result.data();
          this.userHasProfile = result.exists;

        },
      }
    );

   }

  

   onLogOutClick(){

    this.router.navigate(["/"]);

    this.auth.signOut();

   }


   




   loggedIn(){

    return this.auth.isSignedIn();

   }



   isMenuOpen=true;
  

  onToolBarToggle(){
    console.log('On toolbar Toggled', this.isMenuOpen);
    this.isMenuOpen=!this.isMenuOpen;

    
  }


  onCreatePostClick(){
    this.dialog.open(CreatePostComponent);
  }


  

   title = 'VaiolinReal';
   auth = new FirebaseTSAuth();
 
}




export interface UserDocument {
  publicName:string;
  description:string;
}