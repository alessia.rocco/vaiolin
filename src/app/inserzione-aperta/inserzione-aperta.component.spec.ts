import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InserzioneApertaComponent } from './inserzione-aperta.component';

describe('InserzioneApertaComponent', () => {
  let component: InserzioneApertaComponent;
  let fixture: ComponentFixture<InserzioneApertaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InserzioneApertaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InserzioneApertaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
