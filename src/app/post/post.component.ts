import { Component, Input, OnInit } from '@angular/core';
import { PostData } from '../profile/profile.component';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  @Input()postData!: PostData; 

  constructor() { }

  ngOnInit(): void {
  }

}
