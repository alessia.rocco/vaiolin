import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }


  isMenuOpen=false;
  

  onToolBarToggle(){
    console.log('On toolbar Toggled', this.isMenuOpen);
    this.isMenuOpen=!this.isMenuOpen;

    
  }

}
