import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PhotoOpenComponent } from '../photo-open/photo-open.component';

@Component({
  selector: 'app-post-profilo-user',
  templateUrl: './post-profilo-user.component.html',
  styleUrls: ['./post-profilo-user.component.css']
})
export class PostProfiloUserComponent implements OnInit {

  constructor(private dialogTrigger: MatDialog) { }

  ngOnInit(): void {
  }

  clickPhoto(){
    let dialogTriggerRef = this.dialogTrigger.open(PhotoOpenComponent);
  }

}
