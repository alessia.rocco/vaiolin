import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PostProfiloUserComponent } from './post-profilo-user.component';

describe('PostProfiloUserComponent', () => {
  let component: PostProfiloUserComponent;
  let fixture: ComponentFixture<PostProfiloUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PostProfiloUserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostProfiloUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
