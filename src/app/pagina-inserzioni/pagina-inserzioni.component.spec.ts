import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaginaInserzioniComponent } from './pagina-inserzioni.component';

describe('PaginaInserzioniComponent', () => {
  let component: PaginaInserzioniComponent;
  let fixture: ComponentFixture<PaginaInserzioniComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaginaInserzioniComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginaInserzioniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
