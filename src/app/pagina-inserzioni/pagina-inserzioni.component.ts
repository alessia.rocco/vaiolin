import { Component, OnInit } from '@angular/core';
import { Options } from "@angular-slider/ngx-slider";
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-pagina-inserzioni',
  templateUrl: './pagina-inserzioni.component.html',
  styleUrls: ['./pagina-inserzioni.component.css']
})
export class PaginaInserzioniComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }


  model!: NgbDateStruct;

  value: number = 1;
  highValue: number = 24;
  options: Options = {
    floor: 1,
    ceil: 24
  };


  formatLabel(value: number) {
    if (value >= 1000) {
      return Math.round(value / 1000) + 'km';
    }

    return value;
  }


  formatLabel2(value: number) {
    if (value >= 1000) {
      return Math.round(value / 1000) + '€';
    }

    return value;
  }

}
