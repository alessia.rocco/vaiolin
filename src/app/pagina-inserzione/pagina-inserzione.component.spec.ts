import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaginaInserzioneComponent } from './pagina-inserzione.component';

describe('PaginaInserzioneComponent', () => {
  let component: PaginaInserzioneComponent;
  let fixture: ComponentFixture<PaginaInserzioneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaginaInserzioneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginaInserzioneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
