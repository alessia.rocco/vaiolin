import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Offerta1Component } from './offerta1.component';

describe('Offerta1Component', () => {
  let component: Offerta1Component;
  let fixture: ComponentFixture<Offerta1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Offerta1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Offerta1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
