import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClassificaComponent } from './classifica/classifica.component';
import { EmailVerificationComponent } from './email-verification/email-verification.component';
import { EventiComponent } from './eventi/eventi.component';
import { InserzioneApertaComponent } from './inserzione-aperta/inserzione-aperta.component';
import { PaginaEventoComponent } from './pagina-evento/pagina-evento.component';


import { PaginaInizialeComponent } from './pagina-iniziale/pagina-iniziale.component';
import { PaginaInserzioniComponent } from './pagina-inserzioni/pagina-inserzioni.component';
import { PaginaOfferteComponent } from './pagina-offerte/pagina-offerte.component';
import { ProfileComponent } from './profile/profile.component';
import { ProfiloUserComponent } from './profilo-user/profilo-user.component';




const routes: Routes = [

{path:"", component:PaginaInizialeComponent},
{path: "emailVerification", component:EmailVerificationComponent},
{path:"home", component:ProfileComponent},
{path:"classifica", component: ClassificaComponent},
{path: "profiloUser", component: ProfiloUserComponent},
{path: "eventi", component: EventiComponent},
{path: "pagina-evento", component:PaginaEventoComponent},
{path: "pagina-inserzioni", component: PaginaInserzioniComponent},
{path: "pagina-inserzione-aperta", component: InserzioneApertaComponent },
{path: "pagina-offerte", component: PaginaOfferteComponent},




{path:"**", component:PaginaInizialeComponent},


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
