import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FotoEventoCliccataComponent } from './foto-evento-cliccata.component';

describe('FotoEventoCliccataComponent', () => {
  let component: FotoEventoCliccataComponent;
  let fixture: ComponentFixture<FotoEventoCliccataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FotoEventoCliccataComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FotoEventoCliccataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
