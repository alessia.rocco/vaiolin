import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SchedaOffertaFattaComponent } from './scheda-offerta-fatta.component';

describe('SchedaOffertaFattaComponent', () => {
  let component: SchedaOffertaFattaComponent;
  let fixture: ComponentFixture<SchedaOffertaFattaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SchedaOffertaFattaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SchedaOffertaFattaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
