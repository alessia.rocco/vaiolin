import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FotoEventoCliccataComponent } from '../foto-evento-cliccata/foto-evento-cliccata.component';

@Component({
  selector: 'app-pagina-evento',
  templateUrl: './pagina-evento.component.html',
  styleUrls: ['./pagina-evento.component.css']
})
export class PaginaEventoComponent implements OnInit {

  constructor(private dialogTrigger: MatDialog) { }

  ngOnInit(): void {
  }

  onOpenDialogClick(){
    this.dialogTrigger.open(FotoEventoCliccataComponent)
  }

}
