import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SchedaInserzioniComponent } from './scheda-inserzioni.component';

describe('SchedaInserzioniComponent', () => {
  let component: SchedaInserzioniComponent;
  let fixture: ComponentFixture<SchedaInserzioniComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SchedaInserzioniComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SchedaInserzioniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
