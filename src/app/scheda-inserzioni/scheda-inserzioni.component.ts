import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-scheda-inserzioni',
  templateUrl: './scheda-inserzioni.component.html',
  styleUrls: ['./scheda-inserzioni.component.css']
})
export class SchedaInserzioniComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }

  SchedaInsezrzioniCliccata(){
    this.router.navigate(["pagina-inserzione-aperta"]);
  }

}
