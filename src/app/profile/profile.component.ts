
import { Component, Input, OnInit, HostListener  } from '@angular/core';
import { FirebaseTSFirestore, Limit, OrderBy, } from 'firebasets/firebasetsFirestore/firebaseTSFirestore';



@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {


  header_variable = false;

  @HostListener('scroll', ['$event']) 
  scrollHandler(_event: any) {
   
   console.log ("scrollato")
  }
  


  

  firestore= new FirebaseTSFirestore();
  posts:  PostData[] = [];
  @Input() show!: boolean;

 

  constructor() { 

    
  }

  

  ngOnInit(): void {

    this.getPosts();

  }




  getPosts(){
    this.firestore.getCollection(
      {
        path:["Posts"],
        where: [
         
          new OrderBy("timestamp", "desc" ),
          new Limit(10)
        ],
        onComplete: (result) => {

          result.docs.forEach(
            doc=> {
              let post = <PostData> doc.data();
              this.posts.push(post);
            }
          );


        },
        onFail: err =>{

        }
      }
    );
  }
 
}


export interface PostData{
  comment:string;
  cretorId: string;
  ImageUrl?: string;
}



