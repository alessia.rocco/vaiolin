import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-scheda-evento',
  templateUrl: './scheda-evento.component.html',
  styleUrls: ['./scheda-evento.component.css']
})
export class SchedaEventoComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }

  VaiPagineEvento(){

    this.router.navigate(["pagina-evento"]);

  }

 
  

}
