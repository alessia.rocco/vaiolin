import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SchedaEventoComponent } from './scheda-evento.component';

describe('SchedaEventoComponent', () => {
  let component: SchedaEventoComponent;
  let fixture: ComponentFixture<SchedaEventoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SchedaEventoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SchedaEventoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
