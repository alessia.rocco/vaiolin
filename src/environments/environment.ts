// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  firebaseConfig: {
  apiKey: "AIzaSyAaftkBL5nOXuYGRQh7bl6gGTQVkkeT2T8",
  authDomain: "vaiolin-3e081.firebaseapp.com",
  projectId: "vaiolin-3e081",
  storageBucket: "vaiolin-3e081.appspot.com",
  messagingSenderId: "410342577418",
  appId: "1:410342577418:web:10a04b43b9828abef156f3",
  measurementId: "G-CNGG9JMY3S"
  }

};



/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
